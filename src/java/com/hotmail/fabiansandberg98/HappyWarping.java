package com.hotmail.fabiansandberg98;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import com.hotmail.fabiansandberg98.commands.delwarp;
import com.hotmail.fabiansandberg98.commands.setwarp;
import com.hotmail.fabiansandberg98.commands.warp;
import com.hotmail.fabiansandberg98.commands.warps;

public class HappyWarping extends JavaPlugin {
	
	public static HappyWarping instance;
	
	private FileConfiguration warpConfig = null;
	private File warpConfigFile = null;
	
	public void onEnable() {
		instance = this;
		this.createWarpConfig();
		
		// Load config defaults
		getConfig().addDefault("TELEPORT_TO_WARP", "&2You got teleported to %warpname%");
		getConfig().addDefault("CREATE_NEW_WARP", "&2You created a new warp");
		getConfig().addDefault("WARP_LIST", "&2Currently available warps: &f%warplist%");
		getConfig().addDefault("INVALID_WARP", "&4That warp does not exist!");
		getConfig().addDefault("WARP_ALREADY_EXIST", "&4That warp already exists!");
		getConfig().addDefault("DEL_WARP", "&2You deleted a warp");
		getConfig().addDefault("NO_AVAILABLE_WARPS", "&4There is no available warps");
		getConfig().addDefault("WRONG_USE", "&4You typed in the command wrong, the right use is: &f%command%");
		getConfig().addDefault("NO_PERMISSION", "&4You do not have permission to do that");
		getConfig().addDefault("INVALID_CHARACTERS", "&4Invalid name, it can only contain letters and/or numbers");
		saveDefaultConfig();
		
		getLogger().info("HappyWarping is enable!");
		
		warp warp = new warp();
		getCommand("warp").setExecutor(warp);
		
		setwarp setwarp = new setwarp();
		getCommand("setwarp").setExecutor(setwarp);
		
		delwarp delwarp = new delwarp();
		getCommand("delwarp").setExecutor(delwarp);
		
		warps warps = new warps();
		getCommand("warps").setExecutor(warps);
	}
	
	public void onDisable() {
		instance = null;
		getLogger().info("HappyWarping is disable!");
	}
	
	public static HappyWarping getInstance() {
		return instance;
	}
	
	private void createWarpConfig() {
	    if (warpConfigFile == null) {
	    warpConfigFile = new File(getDataFolder(), "warps.yml");
	    getLogger().info("warps.yml will appear when you create a warp..");
	    }
	    warpConfig = YamlConfiguration.loadConfiguration(warpConfigFile);
	 
	    // Look for defaults in the jar
	    Reader defConfigStream = null;
		try {
			defConfigStream = new InputStreamReader(this.getResource("warps.yml"), "UTF8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    if (defConfigStream != null) {
	        YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
	        warpConfig.setDefaults(defConfig);
	    }
	}
	
	public FileConfiguration getWarpConfig() {
	    if (warpConfig == null) {
	        createWarpConfig();
	    }
	    return warpConfig;
	}
	
	public void saveWarpConfig() {
	    if (warpConfig == null || warpConfigFile == null) {
	        return;
	    }
	    try {
	        getWarpConfig().save(warpConfigFile);
	    } catch (IOException ex) {
	        getLogger().log(Level.SEVERE, "Could not save config to " + warpConfigFile, ex);
	    }
	}
}
