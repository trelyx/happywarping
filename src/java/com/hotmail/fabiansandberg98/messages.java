package com.hotmail.fabiansandberg98;


public enum messages {
	
	CONSOLE("can only be used in-game!"),
	WARP_TO_LOC(HappyWarping.getInstance().getConfig().getString("TELEPORT_TO_WARP").replaceAll("(&([a-f0-9]))", "\u00A7$2")),
	WARP_LIST(HappyWarping.getInstance().getConfig().getString("WARP_LIST").replaceAll("(&([a-f0-9]))", "\u00A7$2")),
	WARP_SETWARP(HappyWarping.getInstance().getConfig().getString("CREATE_NEW_WARP").replaceAll("(&([a-f0-9]))", "\u00A7$2")),
	WARP_DOES_NOT_EXIST(HappyWarping.getInstance().getConfig().getString("INVALID_WARP").replaceAll("(&([a-f0-9]))", "\u00A7$2")),
	WARP_ALREADY_EXIST(HappyWarping.getInstance().getConfig().getString("WARP_ALREADY_EXIST").replaceAll("(&([a-f0-9]))", "\u00A7$2")),
	WARP_DEL_WARP(HappyWarping.getInstance().getConfig().getString("DEL_WARP").replaceAll("(&([a-f0-9]))", "\u00A7$2")),
	NO_AVAILABLE_WARPS(HappyWarping.getInstance().getConfig().getString("NO_AVAILABLE_WARPS").replaceAll("(&([a-f0-9]))", "\u00A7$2")),
	WRONG_USE(HappyWarping.getInstance().getConfig().getString("WRONG_USE").replaceAll("(&([a-f0-9]))", "\u00A7$2")),
	INVALID_CHARACTERS(HappyWarping.getInstance().getConfig().getString("INVALID_CHARACTERS").replaceAll("(&([a-f0-9]))", "\u00A7$2")),
	NO_PERMISSION(HappyWarping.getInstance().getConfig().getString("NO_PERMISSION").replaceAll("(&([a-f0-9]))", "\u00A7$2"));
	
	private String msg;
	
	messages (String msg) {
		this.setMsg(msg);
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

}
