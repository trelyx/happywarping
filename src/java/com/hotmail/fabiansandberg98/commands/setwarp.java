package com.hotmail.fabiansandberg98.commands;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.hotmail.fabiansandberg98.HappyWarping;
import com.hotmail.fabiansandberg98.messages;

public class setwarp implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			
			if (player.hasPermission("happywarping.setwarp")) {
				if (args.length == 1) {
					
					if (HappyWarping.getInstance().getWarpConfig().getString("warps." + args[0].toLowerCase()) == null) {				
							if (args[0].matches("[a-zA-Z0-9_ ]*")) {
						
								// Location Strings
							String world = player.getWorld().getName();
							int x = player.getLocation().getBlockX(),
								y = player.getLocation().getBlockY(),
								z = player.getLocation().getBlockZ();
							
							float ya = player.getLocation().getYaw(),
								  pi = player.getLocation().getPitch();
							
							
							// Time Strings
							Date date = new Date();
							
							SimpleDateFormat formatDate1 = new SimpleDateFormat("dd.MM.yyyy");
								// Format
								String formatDate = formatDate1.format(date);
								
								
							SimpleDateFormat formatTime1 = new SimpleDateFormat("HH:mm");
								// Format
							String formatTime = formatTime1.format(date);
							
							
							// Other Strings
							String creator = player.getUniqueId().toString();
							
							// Save and Create warp
							createAll(args, world, x, y, z, ya, pi, creator, formatDate, formatTime);
							HappyWarping.getInstance().saveWarpConfig();
						
							player.sendMessage(messages.WARP_SETWARP.getMsg().replaceAll("%warpname%", args[0].toLowerCase()));
							
							} else {
								player.sendMessage(messages.INVALID_CHARACTERS.getMsg().replaceAll("%warpname%", args[0].toLowerCase()));
						}
					} else {
						player.sendMessage(messages.WARP_ALREADY_EXIST.getMsg().replaceAll("%warpname%", args[0].toLowerCase()).replace("%command%", "/setwarp <name>"));
					}
				} else {
					player.sendMessage(messages.WRONG_USE.getMsg().replaceAll("%command%", "/setwarp <name>"));
				}
			} else {
				player.sendMessage(messages.NO_PERMISSION.getMsg().replaceAll("%command%", "/setwarp <name>"));
			}
		} else {
			sender.sendMessage(messages.CONSOLE.getMsg());
		}
		return false;
	}
	
	/*
	 * Save warp information in config.
	 */
	private void createAll(String[] args, String world, int x, int y, int z, float ya, float pi, String creator, String formatDate, String formatTime) {
		HappyWarping.getInstance().getWarpConfig().set("warps." + args[0].toLowerCase() + ".world", world);
		HappyWarping.getInstance().getWarpConfig().set("warps." + args[0].toLowerCase() + ".x", x);
		HappyWarping.getInstance().getWarpConfig().set("warps." + args[0].toLowerCase() + ".y", y);
		HappyWarping.getInstance().getWarpConfig().set("warps." + args[0].toLowerCase() + ".z", z);
		HappyWarping.getInstance().getWarpConfig().set("warps." + args[0].toLowerCase() + ".ya", ya);
		HappyWarping.getInstance().getWarpConfig().set("warps." + args[0].toLowerCase() + ".pi", pi);
		HappyWarping.getInstance().getWarpConfig().set("warps." + args[0].toLowerCase() + ".creator", creator);
		HappyWarping.getInstance().getWarpConfig().set("warps." + args[0].toLowerCase() + ".date", formatDate);
		HappyWarping.getInstance().getWarpConfig().set("warps." + args[0].toLowerCase() + ".time", formatTime);
	}

}