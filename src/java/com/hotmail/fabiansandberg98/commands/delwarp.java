package com.hotmail.fabiansandberg98.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.hotmail.fabiansandberg98.HappyWarping;
import com.hotmail.fabiansandberg98.messages;

public class delwarp implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			
			if (player.hasPermission("happywarping.warp")) {
				if (args.length == 1) {
					
					if (HappyWarping.getInstance().getWarpConfig().getString("warps." + args[0].toLowerCase()) != null) {
						
						HappyWarping.getInstance().getWarpConfig().set("warps." + args[0].toLowerCase(), null);
						HappyWarping.getInstance().saveWarpConfig();
						player.sendMessage(messages.WARP_DEL_WARP.getMsg().replaceAll("%warpname%", args[0].toLowerCase()));
					} else {
						player.sendMessage(messages.WARP_DOES_NOT_EXIST.getMsg().replaceAll("%command%", "/delwarp <name>").replaceAll("%warpname%", args[0].toLowerCase()));
					}
				} else {
					player.sendMessage(messages.WRONG_USE.getMsg().replaceAll("%command%", "/delwarp <name>"));
				}
			} else {
				player.sendMessage(messages.NO_PERMISSION.getMsg().replaceAll("%command%", "/delwarp <name>"));
			}
		} else {
			sender.sendMessage(messages.CONSOLE.getMsg());
		}
		return false;
	}

}
