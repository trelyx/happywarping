package com.hotmail.fabiansandberg98.commands;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.hotmail.fabiansandberg98.HappyWarping;
import com.hotmail.fabiansandberg98.messages;

public class warp implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			
			if (player.hasPermission("happywarping.warp")) {
				if (args.length == 1) {
					
					if (HappyWarping.getInstance().getWarpConfig().getString("warps." + args[0].toLowerCase()) != null) {
						
						String cworld = HappyWarping.getInstance().getWarpConfig().getString("warps." + args[0].toLowerCase() + ".world");
								int x = HappyWarping.getInstance().getWarpConfig().getInt("warps." + args[0].toLowerCase() + ".x"),
								y = HappyWarping.getInstance().getWarpConfig().getInt("warps." + args[0].toLowerCase() + ".y"),
								z = HappyWarping.getInstance().getWarpConfig().getInt("warps." + args[0].toLowerCase() + ".z");
								
								float ya = HappyWarping.getInstance().getWarpConfig().getInt("warps." + args[0].toLowerCase() + ".ya"),
										pi = HappyWarping.getInstance().getWarpConfig().getInt("warps." + args[0].toLowerCase() + ".pi");
								
								World world = Bukkit.getServer().getWorld(cworld);
						
						player.sendMessage(messages.WARP_TO_LOC.getMsg().replaceAll("%warpname%", args[0].toLowerCase()));
						player.teleport(new Location(world, x, y, z, ya, pi));
					} else {
						player.sendMessage(messages.WARP_DOES_NOT_EXIST.getMsg().replaceAll("%command%", "/warp <name>").replaceAll("%warpname%", args[0].toLowerCase()));
					}
				} else {
					player.sendMessage(messages.WRONG_USE.getMsg().replaceAll("%command%", "/warp <name>"));
				}
			} else {
				player.sendMessage(messages.NO_PERMISSION.getMsg().replaceAll("%command%", "/warp <name>"));
			}
		} else {
			sender.sendMessage(messages.CONSOLE.getMsg());
		}
		return false;
	}

}
