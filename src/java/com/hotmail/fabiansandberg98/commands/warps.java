package com.hotmail.fabiansandberg98.commands;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.hotmail.fabiansandberg98.HappyWarping;
import com.hotmail.fabiansandberg98.messages;

public class warps implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			
			if (player.hasPermission("happywarping.warp")) {
				if (args.length == 0) {
					
					if(HappyWarping.getInstance().getWarpConfig().getString("warps") != null) {
						
						String length = "";
						for (String s : HappyWarping.getInstance().getWarpConfig().getConfigurationSection("warps")
								.getKeys(false)) {
							length = length + "/n" + s;
						}

						if (!(length.length() > 0)) {
							
						player.sendMessage(messages.NO_AVAILABLE_WARPS.getMsg());
						
					} else {
						player.sendMessage(messages.WARP_LIST.getMsg().replaceAll("%warplist%", HappyWarping.getInstance().getWarpConfig().getConfigurationSection("warps")
								.getKeys(false).toString().replace("[", "").replace("]", "")));
					}
					} else {
						player.sendMessage(messages.NO_AVAILABLE_WARPS.getMsg());
					}
					
				} else if (args.length == 1) {
					
					if(HappyWarping.getInstance().getWarpConfig().getString("warps") == null) {
						
						
						player.sendMessage(messages.NO_AVAILABLE_WARPS.getMsg());
						
					} else if (HappyWarping.getInstance().getWarpConfig().getString("warps." + args[0].toLowerCase()) != null) {
						
						String length = "";
						for (String s : HappyWarping.getInstance().getWarpConfig().getConfigurationSection("warps")
								.getKeys(false)) {
							length = length + "/n" + s;
						}

						if (!(length.length() > 0)) {
						
						player.sendMessage(messages.WARP_DOES_NOT_EXIST.getMsg());
						
					} else {
						
						// UUID converter
						String getID = HappyWarping.getInstance().getWarpConfig().getString("warps." + args[0].toLowerCase() + ".creator");
		        		  UUID uuID  = UUID.fromString(getID);
		        		  Player fromID = Bukkit.getPlayer(uuID);
		        		  String creator = fromID.getName();
						
						// ... You can not customize this one <33
						player.sendMessage(ChatColor.DARK_GREEN + "=======( " + ChatColor.WHITE + args[0].toLowerCase() + ChatColor.DARK_GREEN + " )=======\n"
								+ "Created: " + ChatColor.WHITE + HappyWarping.getInstance().getWarpConfig().getString("warps." + args[0].toLowerCase() + ".date")
								+ ChatColor.DARK_GREEN + " / " + ChatColor.WHITE + HappyWarping.getInstance().getWarpConfig().getString("warps." + args[0].toLowerCase() + ".time")
								+ ChatColor.DARK_GREEN + ". by " + ChatColor.WHITE + creator + ChatColor.DARK_GREEN + ".\n"
								
									+ "Location: " + ChatColor.WHITE + HappyWarping.getInstance().getWarpConfig().getString("warps." + args[0].toLowerCase() + ".world").toUpperCase()
									+ ChatColor.DARK_GREEN + ": " + ChatColor.WHITE + "x" + HappyWarping.getInstance().getWarpConfig().getString("warps." + args[0].toLowerCase() + ".x")
									+ ChatColor.DARK_GREEN + ", " + ChatColor.WHITE + "y" + HappyWarping.getInstance().getWarpConfig().getString("warps." + args[0].toLowerCase() + ".y")
									+ ChatColor.DARK_GREEN + ", " + ChatColor.WHITE + "z" + HappyWarping.getInstance().getWarpConfig().getString("warps." + args[0].toLowerCase() + ".z")
									+ ChatColor.DARK_GREEN + ".\n"
											+ "______________________");
					}
					
					} else {
						player.sendMessage(messages.WARP_DOES_NOT_EXIST.getMsg());
					}
				
				} else if (args.length > 1) {
					player.sendMessage(messages.WRONG_USE.getMsg().replaceAll("%command%", "/warps"));
				}
				
			} else {
				player.sendMessage(messages.NO_PERMISSION.getMsg().replaceAll("%command%", "/warps"));
			}
		} else {
			sender.sendMessage(messages.CONSOLE.getMsg());
		}
		return false;
	}

}
